<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/nastavenia', function (Request $request, Response $response, $args) {
    $tplVars['form'] = [
        'uv' => '',
        'dv' => '',
        'ut' => '',
        'dt' => ''
    ];

    try {
        $stmt2 =  $this->db->query('SELECT * FROM settings ORDER BY id');

        $tplVars['settings'] = current_settings($this->db, $this->logger);
        $tplVars['settings_history'] = $stmt2->fetchAll();
        check_auto($this->db, $this->logger);

        return $this->view->render($response, 'settings.latte', $tplVars);
    }
    catch(Exception $e){
        $this->logger->error($e->getMessage());
        die('Aplikacia je momentalne mimo prevoz.');
    }
})->setName('settings');

$app->post('/nastavenia', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();

    if (empty($data['uv']) && empty($data['dv']) && empty($data['ut']) && empty($data['dt'])) {
        $tplVars['error'] = 'Pred odoslaním musíte nastaviť hodnoty';
        $tplVars['form'] = $data;
        return $this->view->render($response, 'settings.latte', $tplVars);
    } else {
        if ( ((!empty($data['uv']) == !empty($data['dv'])) && (!empty($data['ut']) == !empty($data['dt']))) && (($data['uv'] != $data['dv']) || ($data['ut'] != $data['dt'])) && !(!empty($data['uv']) && !empty($data['dt']) && !empty($data['ut']) && !empty($data['dt'])) ) {
            try {
                $this->db->beginTransaction(); //zahajenie transakcie

                $stmt = $this->db->prepare('INSERT INTO settings (hodnota_hore, hodnota_dole, cas_hore, cas_dole, datum) VALUES (:uv, :dv, :ut, :dt, current_timestamp)');
                $stmt->bindvalue(':uv', empty($data['uv']) ? null : $data['uv']);
                $stmt->bindvalue(':dv', empty($data['dv']) ? null : $data['dv']);
                $stmt->bindvalue(':ut', empty($data['ut']) ? null : $data['ut']);
                $stmt->bindvalue(':dt', empty($data['dt']) ? null : $data['dt']);
                $stmt->execute();


                $this->db->commit(); //ukoncenie transakcie
                //presmerovat na vypis
                return $response->withHeader('Location', $this->router->pathFor('settings'));
            } catch (Exception $e) {
                $this->logger->error($e->getMessage());
                die($e->getMessage());
            }
        } else {
            if ((empty($data['uv']) && !empty($data['dv']))) {
                $tplVars['error'] = 'Nebola zadaná hodnota pre vytiahnutie';
            } elseif ((!empty($data['uv']) && empty($data['dv']))) {
                $tplVars['error'] = 'Nebola zadaná hodnota pre spustenie';
            } elseif ((empty($data['ut']) && !empty($data['dt']))) {
                $tplVars['error'] = 'Nebola zadaný čas pre vytiahnutie';
            } elseif ((!empty($data['ut']) && empty($data['dt']))) {
                $tplVars['error'] = 'Nebola zadaný čas pre spustenie';
            } elseif (($data['uv'] == $data['dv']) && !empty($data['uv'])) {
                $tplVars['error'] = 'Hodnota pre spustenie a vytiahnutie nesmie byť rovnaká';
            } elseif (($data['ut'] == $data['dt']) && !empty($data['ut'])) {
                $tplVars['error'] = 'Čas pre spustenie a vytiahnutie nesmie byť rovnaký';
            }elseif (!empty($data['uv']) && !empty($data['dt']) && !empty($data['ut']) && !empty($data['dt'])) {
                $tplVars['error'] = 'Nesmiete zadať obe spôsoby. Vyberte si len jeden';
            } else {
                $tplVars['error'] = 'Nastala chyba';
            }

            $tplVars['form'] = $data;
            return $this->view->render($response, 'settings.latte', $tplVars);
        }
    }
});

$app->get('/api/settings', function (Request $request, Response $response, $args) {
    $stmt =  $this->db->query('SELECT * FROM settings ORDER BY id DESC LIMIT 1');
    $tplVars['settings'] = $stmt->fetchAll();
    $response->write(json_encode($tplVars));

    return $response->withHeader('Content-type', 'Aplication/json')->withStatus(200);
});
