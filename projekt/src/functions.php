<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

function senzor_update(PDO $db, $logger, $hodnota) {
    try {
        $stmt = $db->prepare("insert into senzor_log (hodnota, datum) values (:h, current_timestamp)");
        $stmt->bindvalue(':h', $hodnota);
        $stmt->execute();
        check_for_action_value_auto($db, $logger, $hodnota);
        $db->commit(); //ukoncenie transakcie
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        die($e->getMessage());
    }
}

function current_settings(PDO $db, $logger) {
    try {
        $stmt =  $db->query('SELECT * FROM settings ORDER BY id DESC LIMIT 1');
        return $stmt->fetchAll();
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        die($e->getMessage());
    }
}

function current_state(PDO $db, $logger) {
    try {
        $stmt =  $db->query('SELECT * FROM zaznamnik WHERE vykonane=true ORDER BY id_zaznamnik DESC LIMIT 1');
        return $stmt->fetchAll();
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        die($e->getMessage());
    }
}

function current_senzor_value(PDO $db, $logger) {
    try {
        $stmt =  $db->query('SELECT * FROM senzor_log ORDER BY id DESC LIMIT 1');
        return $stmt->fetchAll();
    } catch (Exception $e) {
        $logger->error($e->getMessage());
        die($e->getMessage());
    }
}

function check_for_action_value_man(PDO $db, $logger, $hodnota) {
    $settings = current_settings($db, $logger);
    $state = current_state($db, $logger);

    if ($state[0]['akcia'] == "Spustenie") {
        if ($hodnota >= $settings[0]['hodnota_hore']) {
            try {
                $stmt = $db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp, 'Vytiahnutie', 'Automaticky', true)");
                $stmt-> execute();
            } catch (Exception $e) {
                $logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
    } else {
        if ($hodnota <= $settings[0]['hodnota_dole']) {
            try {
                $stmt = $db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp, 'Spustenie', 'Automaticky', true)");
                $stmt->execute();
            } catch (Exception $e) {
                $logger->error($e->getMessage());
                die($e->getMessage());
            }
        }
    }
}

function check_for_action_value_auto(PDO $db, $logger, $hodnota) {
    $state = current_state($db, $logger);

    if ($state[0]['sposob'] == 'Automaticky') {
        check_for_action_value_man($db, $logger, $hodnota);
    }
}

function check_for_action_time_man(PDO $db, $logger) {
    $settings = current_settings($db, $logger);
    $state = current_state($db, $logger);
    date_default_timezone_set("Europe/Budapest");

    if (!empty($settings[0]['cas_hore'])) {
        if ($state[0]['akcia'] =="Spustenie") {
            if (($settings[0]['cas_hore'] < date("H:i:s")) && (date("H:i:s") < $settings[0]['cas_dole'])) {
                try {
                    $stmt = $db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp, 'Vytiahnutie', 'Automaticky', true)");
                    $stmt-> execute();
                } catch (Exception $e) {
                    $logger->error($e->getMessage());
                    die($e->getMessage());
                }
            }
        } else {
            if ((($settings[0]['cas_dole'] < date("H:i:s")) && (date("H:i:s") < '23:59:59')) || (('00:00:00' < date("H:i:s")) && (date("H:i:s") < $settings[0]['cas_hore']))) {
                try {
                    $stmt = $db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp, 'Spustenie', 'Automaticky', true)");
                    $stmt->execute();
                } catch (Exception $e) {
                    $logger->error($e->getMessage());
                    die($e->getMessage());
                }
            }
        }
    }
}

function check_for_action_time_auto(PDO $db, $logger) {
    $state = current_state($db, $logger);

    if ($state[0]['sposob'] == 'Automaticky') {
        check_for_action_time_man($db, $logger);
    }
}

function check_auto(PDO $db, $logger) {
    $settings = current_settings($db, $logger);
    $hodnota = current_senzor_value($db, $logger);

    if (!empty($settings[0]['cas_hore']) && !empty($settings[0]['cas_dole'])) {
        check_for_action_value_auto($db, $logger, $hodnota[0]['hodnota']);
    } elseif (!empty($settings[0]['hodnota_hore']) && !empty($settings[0]['hodnota_dole'])) {
        check_for_action_time_auto($db, $logger);
    }
}