<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/stav', function($request, $response, $args) {
    try {
        $stmt =  $this->db->query('SELECT * FROM zaznamnik WHERE vykonane=true ORDER BY id_zaznamnik DESC LIMIT 1');

        $tplVars['status'] = $stmt->fetchAll();
        $tplVars['senzor'] = current_senzor_value($this->db, $this->logger);
        check_auto($this->db, $this->logger);

        return $this->view->render($response, 'status.latte', $tplVars);
    }
    catch(Exception $e){    
        $this->logger->error($e->getMessage());
        die('Aplikacia je momentalne mimo prevoz.');
    }
})->setName('status');

$app->get('/Up', function ($request, $response, $args) {
    try {
        $this->db->beginTransaction(); //zahajenie transakcie
        $stmt = $this->db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp, 'Vytiahnutie', 'Manualne', true)");
        $stmt-> execute();
        $this->db->commit(); //ukoncenie transakcie

        return $response->withHeader('Location', $this->router->pathFor('status'));
    }  catch(Exception $e) {
        $this->db->rollback(); //Vratenie transakcie do povodneho stavu
        //Neznama chyba
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('actionUp');

$app->get('/Down', function($request, $response, $args) {
    try {
        $this->db->beginTransaction(); //zahajenie transakcie
        $stmt = $this->db->prepare("insert into zaznamnik (datum, akcia, sposob, vykonane) values (current_timestamp , 'Spustenie', 'Manualne', true)");
        $stmt->execute();
        $this->db->commit(); //ukoncenie transakcie

        return $response->withHeader('Location', $this->router->pathFor('status'));
    }  catch(Exception $e) {
        $this->db->rollback(); //Vratenie transakcie do povodneho stavu
        //Neznama chyba
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
})->setName('actionDown');


$app->post('/api/measurements', function (Request $request, Response $response, $args) {
    $data = isset($_POST['hodnota']) ? $_POST['hodnota'] : null;
    echo "Hodnota ", $data, " presla v poriadku" ;
    senzor_update($this->db, $this->logger, $data);
});

$app->get('/kontrola', function($request, $response, $args) {
    check_for_action_value_man($this->db, $this->logger);
    check_for_action_time_man($this->db, $this->logger);
    return $response->withHeader('Location', $this->router->pathFor('status'));
})->setName('kontrola');