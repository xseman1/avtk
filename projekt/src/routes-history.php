<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//domaca stranka prihlaseneho účtu
$app->get('/historia', function($request, $response, $args) {
    try {
        $stmt =  $this->db->query('SELECT * FROM zaznamnik ORDER BY id_zaznamnik DESC LIMIT 15');

        $tplVars['record'] = $stmt->fetchAll();
        check_auto($this->db, $this->logger);

        return $this->view->render($response, 'history.latte', $tplVars);
    }
    catch(Exception $e){
        $this->logger->error($e->getMessage());
        die('Aplikacia je momentalne mimo prevoz.');
    }
})->setName('history');

//odstranenie kontaktu
$app->post('/zmazanie_zaznamu', function ($request, $response, $args) {
    $input = $request->getParsedBody();
    try {
        $stmt = $this->db->prepare('DELETE FROM zaznamnik WHERE id_zaznamnik = :idz');
        $stmt->bindValue(':idz', $input['id_zaznamnik']);
        $stmt->execute();
    }
    catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('history'));
})->setName('deleteRecord');