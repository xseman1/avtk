<?php
// source: editHome.latte

use Latte\Runtime as LR;

class Template28faff90c4 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>


<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
?>










<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Edit bydliska osoby<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
<?php
		if (isset($form['id_location'])) {
			?>        <form action="<?php
			echo $router->pathFor("editedHome", ['id' => $form['id_location']]);
?>" method="post">
            <div class="col-6">
                <!--value sa stara o to aby zadane hodnoty ostali vo formulary po chyba-->
                <label>Krajina:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['country']) /* line 13 */ ?>" type="text" name="cr" placeholder="Krajina" class="form-control">
                <br>
                <label>Mesto:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['city']) /* line 16 */ ?>" type="text" name="ct" placeholder="Mesto" class="form-control">
                <br>
                <label>Ulica:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['street_name']) /* line 19 */ ?>" type="text" name="str" placeholder="Ulica" class="form-control">
                <br>
                <label>Cislo:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['street_number']) /* line 22 */ ?>" type="number" name="strn" placeholder="Cislo" class="form-control">
                <br>
                <label>Zip:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['zip']) /* line 25 */ ?>" type="text" name="zip" placeholder="ZIP" class="form-control">
                <br>
                <button type="submit" class="btn btn-primary">Edit</button>
                <br>
                <br>
            </div>
        </form>
<?php
		}
		
	}

}
