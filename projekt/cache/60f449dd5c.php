<?php
// source: addMeeting.latte

use Latte\Runtime as LR;

class Template60f449dd5c extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['l'])) trigger_error('Variable $l overwritten in foreach on line 40');
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridanie schodzky<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("createMeeting");
?>" method="post">
        <div class="col-6">
            <!--value sa stara o to aby zadane hodnoty ostali vo formulary po chyba-->
            <label>Start schodze:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['start']) /* line 11 */ ?>" type="datetime-local" name="start" class="form-control" required>
            <br>
            <label>Popis:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['description']) /* line 14 */ ?>" type="text" name="description" placeholder="popis" class="form-control" required>
            <br>
            <label>Trvanie:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['duration']) /* line 17 */ ?>" type="time" name="duration" class="form-control" required>
            <br>
            <label>Mesto:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['c']) /* line 20 */ ?>" type="text" name="c" placeholder="mesto" class="form-control">
            <br>
            <label>Ulica:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['sna']) /* line 23 */ ?>" type="text" name="sna" placeholder="ulica" class="form-control">
            <br>
            <label>Cislo:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['snu']) /* line 26 */ ?>" type="number" name="snu" class="form-control">
            <br>
            <label>Zip:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['zip']) /* line 29 */ ?>" type="text" name="zip" placeholder="zip" class="form-control">
            <br>
            <label>Krajina:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['ct']) /* line 32 */ ?>" type="text" name="ct" placeholder="krajina" class="form-control">
            <br>
            <label>Nazov:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['n']) /* line 35 */ ?>" type="text" name="n" placeholder="nazov" class="form-control">
            <br>
            <label>Výber adresy</label>
            <select name="id_location" class="form-control">
                <option value="">Neznáma adresa</option>
<?php
		$iterations = 0;
		foreach ($locations as $l) {
			if ($form['id_location'] == $l['id_location']) {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($l['id_location']) /* line 42 */ ?>" selected><?php
				echo LR\Filters::escapeHtmlText($l['city']) /* line 42 */ ?>, <?php echo LR\Filters::escapeHtmlText($l['street_name']) /* line 42 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($l['street_number']) /* line 42 */ ?></option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($l['id_location']) /* line 44 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($l['city']) /* line 44 */ ?>, <?php echo LR\Filters::escapeHtmlText($l['street_name']) /* line 44 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($l['street_number']) /* line 44 */ ?></option>
<?php
			}
			$iterations++;
		}
?>
            </select>
            <br>
            <button type="submit" class="btn btn-primary">Vytvor</button>
            <br>
            <br>
        </div>
    </form>
<?php
	}

}
