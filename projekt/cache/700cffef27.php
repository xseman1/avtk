<?php
// source: add-user.latte

use Latte\Runtime as LR;

class Template700cffef27 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Registracia uzivatela<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("userReg");
?>" method="post">
        <div class="col-6">
                <label>Login</label>
                <input type="text" name="ln" value="<?php echo LR\Filters::escapeHtmlAttr($form['ln']) /* line 10 */ ?>" class="form-control"  required>
            <br>
                <label>Heslo</label>
                <input type="password" name="pw" value="<?php echo LR\Filters::escapeHtmlAttr($form['pw']) /* line 13 */ ?>" class="form-control" required>
            <br>
                <label>Overenie hesla</label>
                <input type="password" name="pwo" value="<?php echo LR\Filters::escapeHtmlAttr($form['pwo']) /* line 16 */ ?>" class="form-control" required>
            <br>
            <button type="submit" class="btn btn-primary">Registrovat</button>
        </div>
    </form>
<?php
	}

}
