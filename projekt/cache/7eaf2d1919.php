<?php
// source: login.latte

use Latte\Runtime as LR;

class Template7eaf2d1919 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Login osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("login");
?>" method="post">
        <div class="col-6">
            <label>Login:</label>
            <input type="text" name="login" placeholder="Login" class="form-control" required>
            <br>
            <label>Heslo:</label>
            <input type="password" name="pass" placeholder="Heslo" class="form-control" required>
            <br>
            <button type="submit" class="btn btn-primary">
                <i class="fa fa-key"></i> Prihlasit</button>
            <br>
            <br>
        </div>
    </form>
<?php
	}

}
