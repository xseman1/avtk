<?php
// source: contact.latte

use Latte\Runtime as LR;

class Template44eab122e0 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['c'])) trigger_error('Variable $c overwritten in foreach on line 19');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Kontakty osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <br>
    <h4 class="col-6" >Kontakty osoby <?php echo LR\Filters::escapeHtmlText($contact[0]['first_name']) /* line 8 */ ?> <?php
		echo LR\Filters::escapeHtmlText($contact[0]['last_name']) /* line 8 */ ?>: </h4>
    <br>
    <div class="col-3">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Typ kontaktu</th>
                <th scope="col">Kontakt</th>
                <th scope="col">Vymazat</th>
            </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($contact as $c) {
?>
                <tr>
                    <td><?php echo LR\Filters::escapeHtmlText($c['name']) /* line 21 */ ?></td>
                    <td><?php echo LR\Filters::escapeHtmlText($c['contact']) /* line 22 */ ?></td>
                    <td>
<?php
			if ($c['id_contact'] != null) {
				?>                        <form action="<?php
				echo $router->pathFor("deleteCon");
?>" method="post" onsubmit="return confirm('Naozaj chcete odstranit kontakt?')">
                            <input type="hidden" name="id_contact" value="<?php echo LR\Filters::escapeHtmlAttr($c['id_contact']) /* line 26 */ ?>">
                            <input type="submit" value="VYMAZAT" class="btn btn-danger">
                        </form>
<?php
			}
?>
                    </td>
                </tr>
<?php
			$iterations++;
		}
?>
                <tr>
                    <td><a href="<?php
		echo $router->pathFor("index");
?>" class="btn btn-primary">Navrat</a></td>
                    <td><a href="<?php
		echo $router->pathFor("add-contact");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($c['id_person'])) /* line 35 */ ?>" class="btn btn-primary">Pridat kontakt</a></td>
                </tr>
        </table>
    </div>
<?php
	}

}
