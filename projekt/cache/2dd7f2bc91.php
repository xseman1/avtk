<?php
// source: relations.latte

use Latte\Runtime as LR;

class Template2dd7f2bc91 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['r'])) trigger_error('Variable $r overwritten in foreach on line 36');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis vztahov<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <br>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <form action="<?php
		echo $router->pathFor("relation");
?>" METHOD="get">
                    <input type="text" name="search" placeholder="Najst osobu" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-primary">Hladaj</button>
                </form>
            </div>
            <div class="col-6">
                <a href="<?php
		echo $router->pathFor("createRel");
?>" type="button" class="btn btn-primary btn-block">Pridat vztah</a>
            </div>
        </div>
    </div>
    <br>

    <div class="col-12 align-self-center">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Meno</th>
                <th scope="col">Priezvisko</th>
                <th scope="col">Vztah</th>
                <th scope="col">Meno</th>
                <th scope="col">Priezvysko</th>
                <th scope="col">Vymazat</th>
            </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($relation as $r) {
?>
                <tr>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($r['fn1']) /* line 39 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($r['ln1']) /* line 42 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($r['name']) /* line 45 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($r['fn2']) /* line 48 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($r['ln2']) /* line 51 */ ?>

                    </td>
                    <td>
                        <form action="<?php
			echo $router->pathFor("deleteRel");
?>" method="post" onsubmit="return confirm('Naozaj chcete odstranit vztah?')">
                            <input type="hidden" name="id_relation" value="<?php echo LR\Filters::escapeHtmlAttr($r['id_relation']) /* line 55 */ ?>">
                            <input type="submit" value="VYMAZAT" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
