<?php
// source: edit.latte

use Latte\Runtime as LR;

class Template8cdc7c46b1 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>


<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
?>










<?php
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Edit osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
<?php
		if (isset($form['id_person'])) {
			?>        <form action="<?php
			echo $router->pathFor("edited", ['id' => $form['id_person']]);
?>" method="post">
            <div class="col-6">
                <!--value sa stara o to aby zadane hodnoty ostali vo formulary po chyba-->
                <label>Krstne meno:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['first_name']) /* line 13 */ ?>" type="text" name="fn" placeholder="Krstne Meno" class="form-control" required>
                <br>
                <label>Priezvisko:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['last_name']) /* line 16 */ ?>" type="text" name="ln" placeholder="Priezvisko" class="form-control" required>
                <br>
                <label>Prezyvka:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['nickname']) /* line 19 */ ?>" type="text" name="nn" placeholder="Prezyvka" class="form-control" required>
                <br>
                <label>Datum narodenia:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['birth_day']) /* line 22 */ ?>" type="date" name="bd" placeholder="Datum narodenia" class="form-control">
                <br>
                <label>Vyska:</label>
                <input value="<?php echo LR\Filters::escapeHtmlAttr($form['height']) /* line 25 */ ?>" type="number" name="h" placeholder="Vyska" class="form-control">
                <br>
                <label>Pohlavie:</label>
                <select name="g" class="form-control">
                    <option value="">Vyberte pohlavie</option>
<?php
			if ($form['gender'] == "male") {
?>
                        <option value="male" selected>Muz</option>
<?php
			}
			else {
?>
                        <option value="male"> Muz</option>
<?php
			}
			if ($form['gender'] == "female") {
?>
                        <option value="female" selected>Zena</option>
<?php
			}
			else {
?>
                        <option value="female">Zena</option>
<?php
			}
?>
                </select>
                <br>
                <button type="submit" class="btn btn-primary">Edit</button>
                <br>
                <br>
            </div>
        </form>
<?php
		}
		
	}

}
