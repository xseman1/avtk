<?php
// source: persons.latte

use Latte\Runtime as LR;

class Template0fa0c27d80 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 40');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <div class="container">
        <div class="row">
            <div class="col-6">
                <form action="<?php
		echo $router->pathFor("index");
?>" METHOD="get">
                    <input type="text" name="search" placeholder="Najst osobu" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-primary">Hladaj</button>
                </form>
            </div>
            <div class="col-6">
                <a href="<?php
		echo $router->pathFor("createPerson");
?>" type="button" class="btn btn-primary btn-block">Pridat osobu</a>
            </div>
            <br>
        </div>
    </div>
    <br>
    <div class="col-12">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Krstne meno</th>
                <th scope="col">Priezvisko</th>
                <th scope="col">Prezyvka</th>
                <th scope="col">Datum narodenia</th>
                <th scope="col">Vyska</th>
                <th scope="col">Pohlavie</th>
                <th scope="col">Kontakty</th>
                <th scope="col">Bydlisko</th>
                <th scope="col">Editovat</th>
                <th scope="col">Vymazat</th>
            </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($persons as $p) {
?>
                <tr>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($p['id_person']) /* line 43 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($p['first_name']) /* line 46 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($p['last_name']) /* line 49 */ ?>

                    </td>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($p['nickname']) /* line 52 */ ?>

                    </td>
                    <td>
<?php
			if ((!empty($p['birth_day']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($p['birth_day']) /* line 56 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($p['height']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($p['height']) /* line 63 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($p['gender']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($p['gender']) /* line 70 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
                        <a href="<?php
			echo $router->pathFor("kontakty");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($p['id_person'])) /* line 76 */ ?>" class="btn btn-primary">ZOBRAZIT</a>
                    </td>
                    <td>
                        <a href="<?php
			echo $router->pathFor("personHome");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($p['id_person'])) /* line 79 */ ?>" class="btn btn-primary">ZOBRAZIT</a>
                    </td>
                    <td>
                        <a href="<?php
			echo $router->pathFor("edit");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($p['id_person'])) /* line 82 */ ?>" class="btn btn-warning">EDIT</a>
                    </td>
                    <td>
                        <form action="<?php
			echo $router->pathFor("delete");
?>" method="post" onsubmit="return confirm('Naozaj chcete zmazat osobu?')">
                            <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($p['id_person']) /* line 86 */ ?>">
                            <input type="submit" value="VYMAZAT" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>


<?php
	}

}
