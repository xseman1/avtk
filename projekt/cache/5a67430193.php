<?php
// source: create-person2.latte

use Latte\Runtime as LR;

class Template5a67430193 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['l'])) trigger_error('Variable $l overwritten in foreach on line 55');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("createPersonAddress");
?>" method="post">
        <div class="col-6">
            <!--value sa stara o to aby zadane hodnoty ostali vo formulary po chyba-->
            <label>Krstne meno:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['fn']) /* line 11 */ ?>" type="text" name="fn" placeholder="Krstne Meno" class="form-control" required>
            <br>
            <label>Priezvysko:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['ln']) /* line 14 */ ?>" type="text" name="ln" placeholder="Priezvisko" class="form-control" required>
            <br>
            <label>Prezyvka:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['nn']) /* line 17 */ ?>" type="text" name="nn" placeholder="Prezyvka" class="form-control" required>
            <br>
            <label>Datum narodenia:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['bd']) /* line 20 */ ?>" type="date" name="bd" placeholder="Datum narodenia" class="form-control">
            <br>
            <label>Vyska:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['h']) /* line 23 */ ?>" type="number" name="h" placeholder="Vyska" class="form-control">
            <br>
            <label>Pohlavie:</label>
            <select name="g" class="form-control">
                <option value="">Vyberte pohlavie</option>
<?php
		if ($form['g'] == "male") {
?>
                    <option value="male" selected>Muz</option>
<?php
		}
		else {
?>
                    <option value="male"> Muz</option>
<?php
		}
		if ($form['g'] == "female") {
?>
                    <option value="female" selected>Zena</option>
<?php
		}
		else {
?>
                    <option value="female">Zena</option>
<?php
		}
?>
            </select>

            <div class="row">
                <div class="col-6">
                    <label>Mesto</label>
                    <input type="text" name="c" class="form-control" placeholder="Mesto">
                    <label>Ulica</label>
                    <input type="text" name="sna" class="form-control" placeholder="Ulica">
                </div>
                <div class="col-6">

                </div>
            </div>

            <label>Adresa</label>
            <select name="id_location" class="form-control">
                <option value="">Neznama adresa</option>
<?php
		$iterations = 0;
		foreach ($location as $l) {
			if ($form['id_location'] == $l['id_location']) {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($l['street_name']) /* line 57 */ ?>" selected>
                            <?php echo LR\Filters::escapeHtmlText($l['city']) /* line 58 */ ?>, <?php echo LR\Filters::escapeHtmlText($l['street_name']) /* line 58 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($l['street_number']) /* line 58 */ ?>

                        </option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($l['id_location']) /* line 61 */ ?>">
                            <?php echo LR\Filters::escapeHtmlText($l['city']) /* line 62 */ ?>, <?php echo LR\Filters::escapeHtmlText($l['street_name']) /* line 62 */ ?>, <?php
				echo LR\Filters::escapeHtmlText($l['street_number']) /* line 62 */ ?>

                        </option>>
<?php
			}
			$iterations++;
		}
?>
            </select>
            <br>
            <button type="submit" class="btn btn-primary">Vytvor</button>
            <br>
            <br>
        </div>
    </form>
<?php
	}

}
