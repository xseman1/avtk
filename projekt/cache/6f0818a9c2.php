<?php
// source: meeting.latte

use Latte\Runtime as LR;

class Template6f0818a9c2 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['m'])) trigger_error('Variable $m overwritten in foreach on line 30');
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis schodzi<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <div class="col-12">
        <br>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <a href="<?php
		echo $router->pathFor("createMeeting");
?>" type="button" class="btn btn-primary btn-lg btn-block">Pridat schodzku</a>
            </div>
            <div class="col-3"></div>
        </div>
        <br>
        <br>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID Schodze</th>
                <th scope="col">Mesto</th>
                <th scope="col">Ulica</th>
                <th scope="col">Cislo ulice</th>
                <th scope="col">Trvanie</th>
                <th scope="col">Popis</th>
                <th scope="col">Ucastnici</th>
                <th scope="col">Odstranit</th>
            </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($meeting as $m) {
?>
                <tr>
                    <td>
<?php
			if ((!empty($m['id_meeting']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['id_meeting']) /* line 34 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($m['city']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['city']) /* line 41 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($m['street_name']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['street_name']) /* line 48 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($m['street_number']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['street_number']) /* line 55 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($m['duration']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['duration']) /* line 62 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($m['description']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($m['description']) /* line 69 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
                        <a href="<?php
			echo $router->pathFor("participants");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($m['id_meeting'])) /* line 75 */ ?>" class="btn btn-primary">ZOBRAZIT</a>
                    </td>
                    <td>
                        <form action="<?php
			echo $router->pathFor("deleteMeeting");
?>" method="post" onsubmit="return confirm('Naozaj chcete odstranit schodzku?')">
                            <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($m['id_meeting']) /* line 79 */ ?>">
                            <input type="submit" value="ODSTRANIT" class="btn btn-danger">
                        </form>
                    </td>
                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
