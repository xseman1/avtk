<?php
// source: layout2.latte

use Latte\Runtime as LR;

class Template96b5895890 extends Latte\Runtime\Template
{

	function main()
	{
		extract($this->params);
?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <meta charset="utf-8">
    <title><?php
		$this->renderBlock('title', $this->params, 'html');
?></title>
    <link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 6 */ ?>/css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 7 */ ?>/css/font-awesome/css/all.min.css">
    <link rel="stylesheet" href="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 8 */ ?>/css/custom.css">
    <script type="text/javascript" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 9 */ ?>/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($basePath)) /* line 10 */ ?>/css/bootstrap/js/bootstrap.bundle.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
		if (isset($this->blockQueue["meta"])) {
			$this->renderBlock('meta', $this->params, 'html');
		}
?>

</head>
<body style="background: #9fcdff">

<nav class="navbar navbar-expand-lg navbar-light bg-primary">
    <a class="navbar-brand" href="<?php
		echo $router->pathFor("welcome");
?>">APV</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <ul class="navbar-nav">
        <li>
<?php
		if (!empty($_SESSION['user'])) {
			?>                <form class="form-inline my-2 my-lg-0" action="<?php
			echo $router->pathFor("logout");
?>" method="post">
                    <button class="btn btn-danger my-2 my-sm-0" type="submit">Logout <?php echo LR\Filters::escapeHtmlText($_SESSION['user']['login']) /* line 25 */ ?></button>
                </form>
<?php
		}
?>

<?php
		if (isset($error)) {
?>
                <p class="alert alert-danger">
                    <?php echo LR\Filters::escapeHtmlText($error) /* line 31 */ ?>

                </p>
<?php
		}
?>

        </li>
    </ul>

</nav>


</div>
<?php
		$this->renderBlock('body', $this->params, 'html');
?>
</body>
</html><?php
		return get_defined_vars();
	}

}
