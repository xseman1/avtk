<?php
// source: create-person.latte

use Latte\Runtime as LR;

class Template258709b29c extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("createPerson");
?>" method="post">
        <div class="col-6">
            <!--value sa stara o to aby zadane hodnoty ostali vo formulary po chyba-->
            <label>Krstne meno:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['fn']) /* line 11 */ ?>" type="text" name="fn" placeholder="Krstne Meno" class="form-control" required>
            <br>
            <label>Priezvysko:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['ln']) /* line 14 */ ?>" type="text" name="ln" placeholder="Priezvisko" class="form-control" required>
            <br>
            <label>Prezyvka:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['nn']) /* line 17 */ ?>" type="text" name="nn" placeholder="Prezyvka" class="form-control" required>
            <br>
            <label>Datum narodenia:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['bd']) /* line 20 */ ?>" type="date" name="bd" placeholder="Datum narodenia" class="form-control">
            <br>
            <label>Vyska:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['h']) /* line 23 */ ?>" type="number" name="h" placeholder="Vyska" class="form-control">
            <br>
            <label>Pohlavie:</label>
            <select name="g" class="form-control">
                <option value="">Vyberte pohlavie</option>
<?php
		if ($form['g'] == "male") {
?>
                    <option value="male" selected>Muz</option>
<?php
		}
		else {
?>
                    <option value="male"> Muz</option>
<?php
		}
		if ($form['g'] == "female") {
?>
                    <option value="female" selected>Zena</option>
<?php
		}
		else {
?>
                    <option value="female">Zena</option>
<?php
		}
?>
            </select>
            <br>
            <button type="submit" class="btn btn-primary">Vytvor</button>
            <br>
            <br>
        </div>
    </form>
<?php
	}

}
