<?php
// source: welcome.latte

use Latte\Runtime as LR;

class Template4be200d54f extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layoutWelcome.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis osob<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <br>
    <div align="center">
        <h1 class="font-italic display-1 ">Vitajte</h1>
    </div>
    <br>
<div align="center">
    <div class="row">
        <div class="col-2">

        </div>
        <div class="col-4">
            <div class="card text-center" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Osoby</h5>
                    <p class="card-text">Zoznam osob s moznostou upravy osob, pridavanim novych uzivatelov, adries, kontaktov a vytvaranim vztahov.</p>
                    <a href="<?php
		echo $router->pathFor("index");
?>" class="btn-primary btn-lg btn-block">Zoznam osob</a>
                </div>
            </div>
        </div>

        <div class="col-4">
            <div class="card text-center" style="width: 18rem;">
                <div class="card-body">
                    <h5 class="card-title">Schodze</h5>
                    <p class="card-text">Zoznam schodzi s moznostou vytvaranim novych schodzok, editovanim schodzi a pridavanie ucastnikov.</p>
                    <a href="<?php
		echo $router->pathFor("meeting");
?>" class="btn-primary btn-lg btn-block">Schodzky</a>
                </div>
            </div>
        </div>
        <div class="col-2">
        </div>
    </div>
</div>

<?php
	}

}
