<?php
// source: participants.latte

use Latte\Runtime as LR;

class Templatead420a4964 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['p'])) trigger_error('Variable $p overwritten in foreach on line 18');
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Ucastnici<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <h4 class="col-6"> Ucastnici schodze: </h4>
    <br>
    <div class="col-3">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Meno</th>
                <th scope="col">Priezvisko</th>
                <th scope="col">Odobrat</th>
            </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($part as $p) {
?>
                    <tr>
                        <td><?php echo LR\Filters::escapeHtmlText($p['id_person']) /* line 20 */ ?></td>
                        <td><?php echo LR\Filters::escapeHtmlText($p['first_name']) /* line 21 */ ?></td>
                        <td><?php echo LR\Filters::escapeHtmlText($p['last_name']) /* line 22 */ ?></td>
                        <td>
                            <form action="<?php
			echo $router->pathFor("deleteParticipant");
?>" method="post" onsubmit="return confirm('Naozaj chcete odobrat ucastnika zo schodzky?')">
                                <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($p['id_person']) /* line 25 */ ?>">
                                <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($p['id_meeting']) /* line 26 */ ?>">
                                <input type="submit" value="VYMAZAT" class="btn btn-danger">
                            </form>
                        </td>
                    </tr>
<?php
			$iterations++;
		}
?>
            <tr>
                <td>
                    <a href="<?php
		echo $router->pathFor("add-Participant");
		?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($p['id_meeting'])) /* line 34 */ ?>" class="btn btn-primary">Pridat ucastnika</a>
                </td>
                <td><a href="<?php
		echo $router->pathFor("meeting");
?>" class="btn btn-primary">Navrat</a></td>
        </table>
    </div>
<?php
	}

}
