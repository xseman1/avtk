<?php
// source: person-home.latte

use Latte\Runtime as LR;

class Templatec8b4c0031b extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Adresa osoby<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <br>
    <h4 class="col-6" >Bydlisko osoby <?php echo LR\Filters::escapeHtmlText($home['first_name']) /* line 8 */ ?> <?php
		echo LR\Filters::escapeHtmlText($home['last_name']) /* line 8 */ ?>: </h4>
    <br>
    <div class="col-6">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Krajina</th>
                <th scope="col">Mesto</th>
                <th scope="col">Ulica</th>
                <th scope="col">Cislo</th>
                <th scope="col">ZIP</th>
            </tr>
            </thead>
<?php
		if ($home['id_location'] != null) {
?>
                <tr>
                    <td><?php echo LR\Filters::escapeHtmlText($home['id_location']) /* line 24 */ ?></td>
                    <td>
<?php
			if ((!empty($home['country']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($home['country']) /* line 27 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($home['city']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($home['city']) /* line 34 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($home['street_name']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($home['street_name']) /* line 41 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($home['street_number']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($home['street_number']) /* line 48 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($home['zip']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($home['zip']) /* line 55 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                </tr>
<?php
		}
		else {
?>
                    <tr><td><echo>Uzivatel nema priradenu adresu</echo></td></tr>
<?php
		}
?>
            <tr>
                <td><a href="<?php
		echo $router->pathFor("index");
?>" class="btn btn-primary">Navrat</a></td>
<?php
		if ($home['id_location'] != null) {
			?>                    <td><a href="<?php
			echo $router->pathFor("editHome");
			?>?id=<?php echo LR\Filters::escapeHtmlAttr(LR\Filters::safeUrl($home['id_location'])) /* line 67 */ ?>" class="btn btn-primary">Zmenit adresu</a></td>
<?php
		}
		else {
			?>                    <td><a href="<?php
			echo $router->pathFor("add-home");
?>" class="btn btn-primary">Pridat adresu</a></td>
<?php
		}
?>
            </tr>
        </table>
    </div>

<?php
	}

}
