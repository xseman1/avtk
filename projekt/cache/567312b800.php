<?php
// source: adresses.latte

use Latte\Runtime as LR;

class Template567312b800 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['l'])) trigger_error('Variable $l overwritten in foreach on line 32');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Vypis adries<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <br>
    <form action="<?php
		echo $router->pathFor("adress");
?>">
        <div class="col-6">
            <input type="text" name="search" placeholder="Najst adresu" class="form-control">
        </div>
        <br>
        <div class="col-6">
            <button type="submit" class="btn btn-primary">Hladaj</button>
        </div>
    </form>
    <br>

    <br>
    <div class="col-12">
        <table class="table">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Mesto</th>
                    <th scope="col">Ulica</th>
                    <th scope="col">Cislo ulice</th>
                    <th scope="col">Krajina</th>
                    <th scope="col">ZIP</th>
                </tr>
            </thead>
<?php
		$iterations = 0;
		foreach ($locations as $l) {
?>
                <tr>
                    <td>
                        <?php echo LR\Filters::escapeHtmlText($l['id_location']) /* line 35 */ ?>

                    </td>
                    <td>
<?php
			if ((!empty($l['city']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($l['city']) /* line 39 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($l['street_name']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($l['street_name']) /* line 46 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($l['street_number']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($l['street_number']) /* line 53 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($l['country']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($l['country']) /* line 60 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                    <td>
<?php
			if ((!empty($l['zip']))) {
				?>                            <?php echo LR\Filters::escapeHtmlText($l['zip']) /* line 67 */ ?>

<?php
			}
			else {
?>
                            <echo>Nezname</echo>
<?php
			}
?>
                    </td>
                </tr>
<?php
			$iterations++;
		}
?>
        </table>
    </div>
<?php
	}

}
