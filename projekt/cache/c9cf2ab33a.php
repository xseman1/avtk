<?php
// source: adresa.latte

use Latte\Runtime as LR;

class Templatec9cf2ab33a extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Adresa osoby<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>

    <br>
    <h4 class="col-6" >Adresa osoby <?php echo LR\Filters::escapeHtmlText($location[0]['first_name']) /* line 8 */ ?> <?php
		echo LR\Filters::escapeHtmlText($location[0]['last_name']) /* line 8 */ ?>: </h4>
    <br>
    <div class="col-6">
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Krajina</th>
                <th scope="col">Mesto</th>
                <th scope="col">Ulica</th>
                <th scope="col">Cislo</th>
            </tr>
            </thead>
            <tr>
                <td><?php echo LR\Filters::escapeHtmlText($location['country']) /* line 21 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($location['city']) /* line 22 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($location['street_name']) /* line 23 */ ?></td>
                <td><?php echo LR\Filters::escapeHtmlText($location['street_number']) /* line 24 */ ?></td>
            </tr>
            <tr>
                <td><a href="<?php
		echo $router->pathFor("index");
?>" class="btn btn-primary">Navrat</a></td>
            </tr>
        </table>
    </div>
<?php
	}

}
