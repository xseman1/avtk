<?php
// source: delete-person.latte

use Latte\Runtime as LR;

class Template338a112dc9 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Odstranenie osoby<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("deletePerson");
?>" method="post">
        <label>ID osoby:</label>
        <input type="text" name="id" value="<?php echo LR\Filters::escapeHtmlAttr($form['id']) /* line 9 */ ?>" class="form-control"  required placeholder="ID osoby">
        <br>
        <button type="submit" class="btn btn-primary btn-lg btn-block"> VYMAZ </button>
    </form>
<?php
	}

}
