<?php
// source: addParticipant.latte

use Latte\Runtime as LR;

class Templatefa3bbc8bf5 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['o'])) trigger_error('Variable $o overwritten in foreach on line 12');
		$this->parentName = "layout2.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridanie ucastnika<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("add-Participant");
?>" method="post">
        <div class="col-6">

            <label>Vyber osoby:</label>
            <select name="id_person" class="form-control">
<?php
		$iterations = 0;
		foreach ($osoby as $o) {
			if ($form['id_person'] == $o['id_person']) {
				?>                        <option selected value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 14 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 14 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 14 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 14 */ ?></option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 16 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 16 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 16 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 16 */ ?></option>
<?php
			}
			$iterations++;
		}
?>
            </select>

            <input type="hidden" name="id_meeting" value="<?php echo LR\Filters::escapeHtmlAttr($meeting['id_meeting']) /* line 21 */ ?>">
            <br>
            <button type="submit" class="btn btn-primary">Pridaj</button>
        </div>
    </form>
<?php
	}

}
