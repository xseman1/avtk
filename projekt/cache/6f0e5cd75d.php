<?php
// source: addHome.latte

use Latte\Runtime as LR;

class Template6f0e5cd75d extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['o'])) trigger_error('Variable $o overwritten in foreach on line 11');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridanie bydliska osobe<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("add-home");
?>" method="post">
        <div class="col-6">
            <label>Vyber osoby:</label>
            <select name="id_person" class="form-control">
<?php
		$iterations = 0;
		foreach ($osoby as $o) {
			if ($form['id_person'] == $o['id_person']) {
				?>                        <option selected value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 13 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 13 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 13 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 13 */ ?></option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 15 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 15 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 15 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 15 */ ?></option>
<?php
			}
			$iterations++;
		}
?>
            </select>

            <label>Krajina:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['cr']) /* line 21 */ ?>" type="text" name="cr" placeholder="Krajina" class="form-control">
            <label>Mesto:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['ct']) /* line 23 */ ?>" type="text" name="ct" placeholder="Mesto" class="form-control">
            <label>Ulica:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['str']) /* line 25 */ ?>" type="text" name="str" placeholder="Ulica" class="form-control">
            <label>Cislo:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['strn']) /* line 27 */ ?>" type="number" name="strn" placeholder="Cislo" class="form-control">
            <label>ZIP:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['zip']) /* line 29 */ ?>" type="text" name="zip" placeholder="Zip" class="form-control">
            <br>
            <button type="submit" class="btn btn-primary">Vytvor</button>
        </div>
    </form>
<?php
	}

}
