<?php
// source: addContact.latte

use Latte\Runtime as LR;

class Template44038ba7db extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['tk'])) trigger_error('Variable $tk overwritten in foreach on line 11');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridanie kontaktu<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("add-contact");
?>" method="post">
        <div class="col-6">
            <label>Typ kontaktu:</label>
            <select name="id_contact_type" class="form-control">
<?php
		$iterations = 0;
		foreach ($typy_kontaktov as $tk) {
			?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($tk['id_contact_type']) /* line 12 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($tk['name']) /* line 12 */ ?></option>
<?php
			$iterations++;
		}
?>
            </select>

            <label>Kontakt:</label>
            <input value="<?php echo LR\Filters::escapeHtmlAttr($form['con']) /* line 17 */ ?>" type="text" name="con" placeholder="Kontakt" class="form-control">
            <br>

            <input type="hidden" name="id_person" value="<?php echo LR\Filters::escapeHtmlAttr($osoby['id_person']) /* line 20 */ ?>">
            <button type="submit" class="btn btn-primary">Pridaj</button>
        </div>
    </form>
<?php
	}

}
