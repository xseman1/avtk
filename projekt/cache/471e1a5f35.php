<?php
// source: addRelation.latte

use Latte\Runtime as LR;

class Template471e1a5f35 extends Latte\Runtime\Template
{
	public $blocks = [
		'title' => 'blockTitle',
		'body' => 'blockBody',
	];

	public $blockTypes = [
		'title' => 'html',
		'body' => 'html',
	];


	function main()
	{
		extract($this->params);
?>

<?php
		if ($this->getParentName()) return get_defined_vars();
		$this->renderBlock('title', get_defined_vars());
?>

<?php
		$this->renderBlock('body', get_defined_vars());
		return get_defined_vars();
	}


	function prepare()
	{
		extract($this->params);
		if (isset($this->params['o'])) trigger_error('Variable $o overwritten in foreach on line 11, 29');
		if (isset($this->params['tv'])) trigger_error('Variable $tv overwritten in foreach on line 22');
		$this->parentName = "layout.latte";
		
	}


	function blockTitle($_args)
	{
		?>Pridanie vztahu<?php
	}


	function blockBody($_args)
	{
		extract($_args);
?>
    <br>
    <form action="<?php
		echo $router->pathFor("createRel");
?>" method="post">
        <div class="col-6">
            <label>Prva osoba:</label>
            <select name="id_person1" class="form-control">
<?php
		$iterations = 0;
		foreach ($osoby as $o) {
			if ($form['id_person1'] == $o['id_person']) {
				?>                        <option selected value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 13 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 13 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 13 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 13 */ ?></option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 15 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 15 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 15 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 15 */ ?></option>
<?php
			}
			$iterations++;
		}
?>
            </select>
            <br>
            <label>Typ vztahu:</label>
            <select name="id_relation_type" class="form-control">
<?php
		$iterations = 0;
		foreach ($typy_vztahov as $tv) {
			?>                    <option value="<?php echo LR\Filters::escapeHtmlAttr($tv['id_relation_type']) /* line 23 */ ?>"><?php
			echo LR\Filters::escapeHtmlText($tv['name']) /* line 23 */ ?></option>
<?php
			$iterations++;
		}
?>
            </select>
            <br>
            <label>Druha osoba:</label>
            <select name="id_person2" class="form-control">
<?php
		$iterations = 0;
		foreach ($osoby as $o) {
			if ($form['id_person2'] == $o['id_person']) {
				?>                        <option selected value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 31 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 31 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 31 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 31 */ ?></option>
<?php
			}
			else {
				?>                        <option value="<?php echo LR\Filters::escapeHtmlAttr($o['id_person']) /* line 33 */ ?>"><?php
				echo LR\Filters::escapeHtmlText($o['id_person']) /* line 33 */ ?> <?php echo LR\Filters::escapeHtmlText($o['first_name']) /* line 33 */ ?> <?php
				echo LR\Filters::escapeHtmlText($o['last_name']) /* line 33 */ ?></option>
<?php
			}
			$iterations++;
		}
?>
            </select>
            <br>
            <button type="submit" class="btn btn-primary">Vytvor</button>
        </div>
    </form>
<?php
	}

}
